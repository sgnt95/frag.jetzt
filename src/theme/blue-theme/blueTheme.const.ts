export const blue = {

  '--primary' : '#fff024',
  '--primary-variant': '#30305f',

  '--secondary': '#008f24',
  '--secondary-variant': '#00a83e',

  '--background': '#000024',
  '--surface': '#151531',
  '--dialog': '#373a4f',
  '--cancel': '#262c3c',
  '--alt-surface': '#12122a',
  '--alt-dialog': '#455a64',

  '--on-primary': '#000000',
  '--on-secondary': '#000000',
  '--on-background': '#ffffff',
  '--on-surface': '#FFFFFF',
  '--on-cancel': '#FFFFFF',

  '--green': '#02b409',
  '--red': '#EB190A',
  '--yellow': '#D6A400',
  '--blue': '#263cad',
  '--purple': '#9c27b0',
  '--light-green': '#80ba24',
  '--grey': '#BDBDBD',
  '--grey-light': '#EEEEEE',
  '--black': '#212121',
  '--moderator': '#151531'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Dark Blue Mode',
      'de': 'Dark Blue Mode'
    },
    'description': {
      'en': 'dark blue background',
      'de': 'dunkelblauer Hintergrund'
    }
  },
  'isDark': true,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'background'

};
